package com.cleo.labs.authenticator.anonymous;

import com.cleo.security.hashing.Authenticator;

public class AnonymousAuthenticator implements Authenticator {
    public static final String SCHEME = "anonymous";

    public String getSchemeName() {
        return SCHEME;
    }

    public boolean verify(String username, String password, String hashScheme) {
        return true;
    }
}
