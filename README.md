# Anonymous Authenticator #

This is a custom password matching class for VersaLex intended for use
where anonymous authentication is desired.  This implementation accepts
any password as valid.  It could be extended, for example, to verify that
the password supplied has the form of an email address.

# Installation #

1. Clone the project and build `anonymous-authenticator-0.0.1-SNAPSHOT.jar` using `mvn package`.
2. Copy `anonymous-authenticator-0.0.1-SNAPSHOT.jar` to `lib/api`.
3. Edit `conf/system.properties` and add `cleo.password.validator.anonymous=com.cleo.labs.authenticator.anonymous.AnonymousAuthenticator`
4. Stop and restart your VersaLex server

# Use #

To create an anonymous user, i.e. a user whose authentication credentials will always be
accepted, first create the user by whatever means is comfortable.  Then update the user's
password to `anonymous:2:3:4` using the API or by manually editing the host XML file
containing the user's mailbox definition.  Note that the encoded password must contain
four colon-separated fields and the first one must by `anonymous`, but the remaining fields
are ignored (`2:3:4` in this example).

The selected user can now login with any password.  Use as intended.
